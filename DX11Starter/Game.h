#pragma once

#include "DXCore.h"
#include "SimpleShader.h"
#include "Mesh.h"
#include "Scene.h"
#include <DirectXMath.h>

class Game : public DXCore {

public:
  Game(HINSTANCE hInstance);
  ~Game();

  // Overridden setup and game loop methods.
  void Init();
  void Update(float deltaTime, float totalTime);
  void Draw(float deltaTime, float totalTime);

  // Overridden mouse input helper methods.
  void OnMouseDown(WPARAM buttonState, int x, int y);
  void OnMouseUp(WPARAM buttonState, int x, int y);
  void OnMouseMove(WPARAM buttonState, int x, int y);
  void OnMouseWheel(float wheelDelta, int x, int y);

private:

  void DrawScene(const dg::Scene& scene);

  // Current scene.
  std::unique_ptr<dg::Scene> scene;

  // Keeps track of the old mouse position.  Useful for 
  // determining how far the mouse moved in a single frame.
  POINT prevMousePos;
};

