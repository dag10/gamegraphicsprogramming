//
//  Shader.cpp
//

#include <Shader.h>
#include <Exceptions.h>
#include <Utils.h>
#include <memory>
#include <glm/gtc/type_ptr.hpp>

// For the DirectX Math library
using namespace DirectX;

dg::Shader dg::Shader::FromFiles(
  const std::string& vertexPath, const std::string& fragmentPath) {
  Shader shader;
  shader.vertexPath = vertexPath;
  shader.fragmentPath = fragmentPath;

  shader.vertexShader = std::make_shared<SimpleVertexShader>();
  auto wVertexPath = ToLPCWSTR(vertexPath);
  if (!shader.vertexShader->LoadShaderFile(wVertexPath.data())) {
    throw ShaderLoadException(vertexPath);
  }

  shader.pixelShader = std::make_shared<SimplePixelShader>();
  auto wPixelPath = ToLPCWSTR(fragmentPath);
  if (!shader.pixelShader->LoadShaderFile(wPixelPath.data())) {
    throw ShaderLoadException(fragmentPath);
  }

  return shader;
}

dg::Shader::Shader(dg::Shader&& other) {
  *this = std::move(other);
}

dg::Shader& dg::Shader::operator=(dg::Shader&& other) {
  swap(*this, other);
  return *this;
}

void dg::swap(Shader& first, Shader& second) {
  using std::swap;
  swap(first.vertexPath, second.vertexPath);
  swap(first.fragmentPath, second.fragmentPath);
  swap(first.vertexShader, second.vertexShader);
  swap(first.pixelShader, second.pixelShader);
}

void dg::Shader::Use() {
  vertexShader->SetShader();
  pixelShader->SetShader();
  vertexShader->CopyAllBufferData();
  pixelShader->CopyAllBufferData();
}

void dg::Shader::SetBool(const std::string& name, bool value) {
  vertexShader->SetInt(name, value);
  pixelShader->SetInt(name, value);
}

void dg::Shader::SetInt(const std::string& name, int value) {
  vertexShader->SetInt(name, value);
  pixelShader->SetInt(name, value);
}

void dg::Shader::SetFloat(const std::string& name, float value) {
  vertexShader->SetFloat(name, value);
  pixelShader->SetFloat(name, value);
}

void dg::Shader::SetVec2(
  const std::string& name, const glm::vec2& value) {
  vertexShader->SetFloat2(name, (XMFLOAT2&)value);
  pixelShader->SetFloat2(name, (XMFLOAT2&)value);
}

void dg::Shader::SetVec3(
  const std::string& name, const glm::vec3& value) {
  vertexShader->SetFloat3(name, (XMFLOAT3&)value);
  pixelShader->SetFloat3(name, (XMFLOAT3&)value);
}

void dg::Shader::SetVec4(const std::string& name, const glm::vec4& value) {
  vertexShader->SetFloat4(name, (XMFLOAT4&)value);
  pixelShader->SetFloat4(name, (XMFLOAT4&)value);
}

void dg::Shader::SetMat4(const std::string& name, const glm::mat4& mat) {
  // We don't transpose because the shader expects column-major matrices.
  vertexShader->SetMatrix4x4(name, glm::value_ptr(mat));
  pixelShader->SetMatrix4x4(name, glm::value_ptr(mat));
}

void dg::Shader::SetMat4(const std::string& name, const XMFLOAT4X4& mat) {
  // Tranpose from row-major to column-major for shader.
  XMFLOAT4X4 transposed;
  XMStoreFloat4x4(&transposed, XMMatrixTranspose(XMLoadFloat4x4(&mat)));
  vertexShader->SetMatrix4x4(name, transposed);
  pixelShader->SetMatrix4x4(name, transposed);
}

void dg::Shader::SetMat4(
  const std::string& name, const dg::Transform& xf) {
  SetMat4(name, xf.ToMat4());
}
