//
//  Scene.h
//

#pragma once

#include <SceneObject.h>
#include <Camera.h>
#include <Lights.h>
#include <memory>

namespace dg {

  class Scene : public SceneObject {

  public:

    Scene();

    virtual void Initialize();
    virtual void Update();

    std::shared_ptr<Camera> GetCamera() const;

  protected:

    std::shared_ptr<Camera> mainCamera;
    std::shared_ptr<SceneObject> lightContainer;

    // Robot joints.
    std::shared_ptr<SceneObject> robot;
    std::shared_ptr<SceneObject> leftShoulder;
    std::shared_ptr<SceneObject> rightShoulder;
    std::shared_ptr<SceneObject> leftElbow;
    std::shared_ptr<SceneObject> rightElbow;
    std::shared_ptr<SceneObject> neck;
    std::shared_ptr<SceneObject> eyes;

    // Robot state.
    float nextBlink = -1;
    float endOfBlink = -1;

  }; // class Scene

} // namespace dg
