//
//  Material.h
//

#pragma once

#include <memory>
#include <unordered_map>
#include <Shader.h>
#include <Lights.h>

namespace dg {

  enum MaterialPropertyType {
    PROPERTY_NULL,

    PROPERTY_BOOL,
    PROPERTY_INT,
    PROPERTY_FLOAT,
    PROPERTY_VEC2,
    PROPERTY_VEC3,
    PROPERTY_VEC4,
    PROPERTY_MAT4X4,

    PROPERTY_MAX,
  };

  union MaterialPropertyValue {
    bool _bool;
    int _int;
    float _float;
    glm::vec2 _vec2;
    glm::vec3 _vec3;
    glm::vec4 _vec4;
    glm::mat4x4 _mat4x4;

    MaterialPropertyValue() {
      memset(this, 0, sizeof(MaterialPropertyValue));
    }
  };

  // Same as ShaderProperty, except it can own the texture.
  struct MaterialProperty {
    MaterialPropertyType type = PROPERTY_NULL;
    MaterialPropertyValue value;
  };

  class Material {

  public:

    Material() = default;

    Material(Material& other);
    Material(Material&& other);
    Material& operator=(Material& other);
    Material& operator=(Material&& other);
    friend void swap(Material& first, Material& second); // nothrow

    std::shared_ptr<Shader> shader = nullptr;

    void SetProperty(const std::string& name, bool value);
    void SetProperty(const std::string& name, int value);
    void SetProperty(const std::string& name, float value);
    void SetProperty(const std::string& name, glm::vec2 value);
    void SetProperty(const std::string& name, glm::vec3 value);
    void SetProperty(const std::string& name, glm::vec4 value);
    void SetProperty(const std::string& name, glm::mat4x4 value);

    void ClearProperty(const std::string& name);

    void SendCameraPosition(glm::vec3 position);
    void SendLights(const Light::ShaderData(&lights)[Light::MAX_LIGHTS]);
    void SendMatrixMVP(glm::mat4x4 mvp);
    void SendMatrixM(glm::mat4x4 m);
    void SendMatrixNormal(glm::mat4x4 normal);

    void Use() const;

  private:

    std::unordered_map<std::string, MaterialProperty> properties;

  }; // class Material

} // namespace dg
