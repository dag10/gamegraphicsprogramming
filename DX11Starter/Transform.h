//
//  Transform.h
//
#pragma once

#include <DirectXMath.h>
#include <glm/glm.hpp>
#include <string>
#include <ostream>

namespace dg {
	using DirectX::XMFLOAT3;
	using DirectX::XMFLOAT4;
	using DirectX::XMMATRIX;

	static const DirectX::XMFLOAT3 X_BASIS {  1,  0,  0 };
	static const DirectX::XMFLOAT3 Y_BASIS {  0,  1,  0 };
	static const DirectX::XMFLOAT3 Z_BASIS {  0,  0,  1 };
	static const DirectX::XMFLOAT3 RIGHT   {  1,  0,  0 };
	static const DirectX::XMFLOAT3 UP      {  0,  1,  0 };
	static const DirectX::XMFLOAT3 FORWARD {  0,  0, -1 };

	struct Transform {

		static Transform T(DirectX::XMFLOAT3 translation);
		static Transform R(DirectX::XMFLOAT4 rotation);
		static Transform S(DirectX::XMFLOAT3 scale);

		static Transform TR(
			DirectX::XMFLOAT3 translation, DirectX::XMFLOAT4 rotation);
		static Transform RS(
			DirectX::XMFLOAT4 rotation, DirectX::XMFLOAT3 scale);
		static Transform TS(
			DirectX::XMFLOAT3 translation, DirectX::XMFLOAT3 scale);

		static Transform TRS(
			DirectX::XMFLOAT3 translation, DirectX::XMFLOAT4 rotation,
			DirectX::XMFLOAT3 scale);

		static DirectX::XMFLOAT4 EulerToQuatRad(DirectX::XMFLOAT3 euler);
		static DirectX::XMFLOAT4 EulerToQuatDeg(DirectX::XMFLOAT3 euler);

		Transform() = default;

		DirectX::XMFLOAT3 translation = DirectX::XMFLOAT3{ 0, 0, 0 };
		DirectX::XMFLOAT4 rotation = DirectX::XMFLOAT4{ 0, 0, 0, 1 };
		DirectX::XMFLOAT3 scale = DirectX::XMFLOAT3{ 1, 1, 1 };

		Transform Inverse() const;
		friend Transform operator*(const Transform& a, const Transform& b);
		friend inline DirectX::XMMATRIX operator*(
			const DirectX::XMMATRIX& a, const Transform& b) {
			return a * b.ToXMMatrix();
		}
		friend inline DirectX::XMMATRIX operator*(
			const Transform& a, const DirectX::XMMATRIX& b) {
			return a.ToXMMatrix() * b;
		}

		glm::mat4x4 ToMat4() const;
		DirectX::XMFLOAT4X4 ToFloat4X4() const;
		inline DirectX::XMMATRIX ToXMMatrix() const {
			DirectX::XMMATRIX t = DirectX::XMMatrixTranslationFromVector(
				DirectX::XMLoadFloat3(&translation));
			DirectX::XMMATRIX r = DirectX::XMMatrixRotationQuaternion(
				DirectX::XMLoadFloat4(&rotation));
			DirectX::XMMATRIX s = DirectX::XMMatrixScalingFromVector(
				DirectX::XMLoadFloat3(&scale));
			return s * r * t;
		};

		DirectX::XMFLOAT3 Right() const;
		DirectX::XMFLOAT3 Up() const;
		DirectX::XMFLOAT3 Forward() const;

		std::string ToString() const;
		friend std::ostream& operator<<(
			std::ostream& os, const dg::Transform& xf);

	}; // struct Transform

} // namespace dg
