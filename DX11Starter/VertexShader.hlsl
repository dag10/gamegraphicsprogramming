cbuffer externalData : register(b0) {
  matrix model;
  matrix view;
  matrix projection;
};

struct VertexShaderInput {
  float3 position		: POSITION;
  float3 normal		: NORMAL;
  float3 texCoord		: TEXCOORD;
};

struct VertexToPixel {
  float4 position		: SV_POSITION;
  float4 normal		: NORMAL;
  float2 texCoord		: TEXCOORD;
};

VertexToPixel main(VertexShaderInput input) {
  VertexToPixel output;

  matrix worldViewProj = mul(mul(model, view), projection);

  output.position = mul(float4(input.position, 1.0f), worldViewProj);

  // TODO: Transform this according to M matrix
  //output.normal = input.normal;

  output.texCoord = input.texCoord;

  return output;
}
