//
//  Model.h
//

#pragma once

#include <memory>
#include <glm/glm.hpp>
#include <SceneObject.h>
#include <Mesh.h>
#include <Material.h>
#include <Lights.h>

namespace dg {

  class Model : public SceneObject {

  public:

    Model();

    Model(
      std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material,
      Transform transform);

    Model(Model& other);

    std::shared_ptr<Mesh> mesh = nullptr;
    std::shared_ptr<Material> material = nullptr;

    void Draw(
      const glm::mat4x4& view, const glm::mat4x4& projection,
      const glm::vec3& cameraPosition,
      const Light::ShaderData(&lights)[Light::MAX_LIGHTS]) const;

  }; // class Model

} // namespace dg
