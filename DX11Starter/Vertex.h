#pragma once

#include <DirectXMath.h>

// For the DirectX Math library
using namespace DirectX;

// Cast a float to an int with identical bits. Does not cast numerical value.
static int FloatToIntBitwise(float f) {
  union {
    int i;
    float f;
  } u;

  u.f = f;
  return u.i;
}

// Hashes a DirectXMath vector.
static int HashVector(DirectX::XMFLOAT2 v) {
  return
    FloatToIntBitwise(v.x) ^
    FloatToIntBitwise(v.y);
}

// Hashes a DirectXMath vector.
static int HashVector(DirectX::XMFLOAT3 v) {
  return
    FloatToIntBitwise(v.x) ^
    FloatToIntBitwise(v.y) ^
    FloatToIntBitwise(v.z);
}

// Hashes a DirectXMath vector.
static int HashVector(DirectX::XMFLOAT4 v) {
  return
    FloatToIntBitwise(v.x) ^
    FloatToIntBitwise(v.y) ^
    FloatToIntBitwise(v.z) ^
    FloatToIntBitwise(v.w);
}

// Compares DirectXMath vectors by absolute equality, no epsilon.
static bool VectorsEqual(DirectX::XMFLOAT2 a, DirectX::XMFLOAT2 b) {
  return (a.x == b.x) && (a.y == b.y);
}

// Compares DirectXMath vectors by absolute equality, no epsilon.
static bool VectorsEqual(DirectX::XMFLOAT3 a, DirectX::XMFLOAT3 b) {
  return (a.x == b.x) && (a.y == b.y) && (a.z == b.z);
}

// Compares DirectXMath vectors by absolute equality, no epsilon.
static bool VectorsEqual(DirectX::XMFLOAT4 a, DirectX::XMFLOAT4 b) {
  return (a.x == b.x) && (a.y == b.y) && (a.z == b.z) && (a.w == b.w);
}

static XMFLOAT2 VectorToFloat2(XMVECTOR vector) {
  XMFLOAT2 ret;
  XMStoreFloat2(&ret, vector);
  return ret;
}

static XMFLOAT4 VectorToFloat4(XMVECTOR vector) {
  XMFLOAT4 ret;
  XMStoreFloat4(&ret, vector);
  return ret;
}

// --------------------------------------------------------
// A custom vertex definition
// --------------------------------------------------------
struct Vertex {
  DirectX::XMFLOAT3 position;
  DirectX::XMFLOAT3 normal;
  DirectX::XMFLOAT2 texCoord;

  Vertex(XMFLOAT3 position, XMFLOAT3 normal, XMFLOAT2 texCoord)
    : position(position), normal(normal), texCoord(texCoord) {}

  Vertex(XMFLOAT3 position, XMFLOAT2 texCoord)
    : position(position), texCoord(texCoord) {}

  Vertex(XMFLOAT4 position, XMFLOAT2 texCoord) : texCoord(texCoord) {
    this->position.x = position.x / position.w;
    this->position.y = position.y / position.w;
    this->position.z = position.z / position.w;
  }

  bool Vertex::operator==(const Vertex &other) const {
    return
      VectorsEqual(position, other.position) &&
      VectorsEqual(normal, other.normal) &&
      VectorsEqual(texCoord, other.texCoord);
  }

  struct hash {
    int operator()(const Vertex& v) const {
      return
        HashVector(v.position) ^
        HashVector(v.normal) ^
        HashVector(v.texCoord);
    }
  };
};
