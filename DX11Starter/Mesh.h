#pragma once

#include "Vertex.h"
#include "DXCore.h"
#include <vector>
#include <unordered_map>
#include <memory>

namespace dg {

  // A resource to contain a 3D mesh, a sequence of vertices that form triangles.
  // Also contains functions to help create a mesh by adding triangles or quads
  // and then sending the mesh to the GPU.
  //
  // Copy is disabled, only moves are allowed. This prevents us
  // from leaking, accidentally sharing, or prematurely releasing
  // DirectX resources.
  class Mesh {

  public:

    enum class Winding {
      CW,  // Standard for DirectX
      CCW, // Standard for OpenGL and this engine.
    };

    static std::shared_ptr<Mesh> Quad;
    static std::shared_ptr<Mesh> Cube;
    static std::shared_ptr<Mesh> Cylinder;

    static void CreateDefaultMeshes(
      ID3D11Device *device, ID3D11DeviceContext *context);

    static std::shared_ptr<Mesh> LoadOBJ(const char *filename);

    Mesh(ID3D11Device *device, ID3D11DeviceContext *context);
    Mesh(Mesh&& other);
    ~Mesh();
    Mesh& operator=(Mesh& other) = delete;
    Mesh& operator=(Mesh&& other);
    friend void swap(Mesh& first, Mesh& second); // nothrow

    // Draws the mesh on the device context.
    void Draw() const;

  private:

    static std::shared_ptr<Mesh> CreateQuad(
      ID3D11Device *device, ID3D11DeviceContext *context);
    static std::shared_ptr<Mesh> CreateCube(
      ID3D11Device *device, ID3D11DeviceContext *context);
    static std::shared_ptr<Mesh> CreateCylinder(
      ID3D11Device *device, ID3D11DeviceContext *context, int subdivisions);

    static std::unordered_map<std::string, std::weak_ptr<Mesh>> fileMap;

    // Adds a quad to the mesh currently being built.
    void AddQuad(
      Vertex v1, Vertex v2, Vertex v3, Vertex v4, Winding winding);

    // Adds a triangle to the mesh currently being built.
    void AddTriangle(Vertex v1, Vertex v2, Vertex v3, Winding winding);

    // Adds a vertex to the mesh currently being built.
    void AddVertex(Vertex vertex);

    // Sends the vertex and index buffers to the GPU, and ends building.
    void FinishBuilding();

    // The DirectX device context this mesh exists on.
    ID3D11Device *device;
    ID3D11DeviceContext *context;

    // Ordered list of Vertex structs. Will be empty once vertex buffer
    // is created.
    std::vector<Vertex> vertices;

    // Map of Vertex structs to index in vertices vector above.
    // Used to quickly determine if vertex already exists when adding
    // a new vertex+index. Will be empty once the vertex and index buffers
    // are created.
    std::unordered_map<Vertex, int, Vertex::hash> vertexMap;

    // Ordered list of indices to a position within the vertices list above.
    // Will be empty once the index buffer is created.
    std::vector<int> indices;

    // Number of indices stored in the index buffer. Only populated once
    // mesh is built.
    int numIndices = -1;

    // Handles to DirectX buffers holding the vertices and indices in the GPU.
    ID3D11Buffer *vertexBuffer = nullptr;
    ID3D11Buffer *indexBuffer = nullptr;
  }; // class Mesh

} // namespace dg
