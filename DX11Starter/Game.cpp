#include "Game.h"
#include "Vertex.h"
#include "Transform.h"
#include "Model.h"
#include "EngineTime.h"
#include "Lights.h"
#include <forward_list>

// For the DirectX Math library
using namespace DirectX;
using namespace dg;

// --------------------------------------------------------
// Constructor
//
// DXCore (base class) constructor will set up underlying fields.
// DirectX itself, and our window, are not ready yet!
//
// hInstance - the application's OS-level handle (unique ID)
// --------------------------------------------------------
Game::Game(HINSTANCE hInstance)
  : DXCore(
    hInstance,		   // The application's handle
    "DirectX Game",	   // Text for the window's title bar
    1280,			   // Width of the window's client area
    720,			   // Height of the window's client area
    true)			   // Show extra stats (fps) in title bar?
{
#if defined(DEBUG) || defined(_DEBUG)
  // Do we want a console window?  Probably only in debug mode
  CreateConsoleWindow(500, 120, 32, 120);
#endif

}

// --------------------------------------------------------
// Destructor - Clean up anything our game has created:
//  - Release all DirectX objects created here
//  - Delete any objects to prevent memory leaks
// --------------------------------------------------------
Game::~Game()
{
}

// --------------------------------------------------------
// Called once per program, after DirectX and the window
// are initialized but before the game loop.
// --------------------------------------------------------
void Game::Init()
{
  // Tell the input assembler stage of the pipeline what kind of
  // geometric primitives (points, lines or triangles) we want to draw.  
  // Essentially: "What kind of shape should the GPU draw with our data?"
  context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

  // Create built-in meshes such as cubes, cylinders, quads, etc.
  Mesh::CreateDefaultMeshes(device, context);

  // Create and initialize the scene.
  scene = std::unique_ptr<Scene>(new Scene());
  scene->Initialize();
}

// --------------------------------------------------------
// Update your game here - user input, move objects, AI, etc.
// --------------------------------------------------------
void Game::Update(float deltaTime, float totalTime)
{
  // Quit if the ESC or Q is pressed.
  if (GetAsyncKeyState(VK_ESCAPE) || GetAsyncKeyState('Q')) {
    Quit();
  }

  Time::Delta = deltaTime;
  Time::Elapsed = totalTime;

  scene->Update();
}

// --------------------------------------------------------
// Clear the screen, redraw everything, present to the user
// --------------------------------------------------------
void Game::Draw(float deltaTime, float totalTime)
{
  // Background color (Cornflower Blue in this case) for clearing.
  const float color[4] = { 0.4f, 0.6f, 0.75f, 0.0f };

  // Clear the render target and depth + stencil buffer.
  context->ClearRenderTargetView(backBufferRTV, color);
  context->ClearDepthStencilView(
    depthStencilView,
    D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
    1.0f,
    0);

  // Draw the scene hierarchy.
  DrawScene(*scene.get());

  // Present the back buffer to the user.
  swapChain->Present(0, 0);
}

// --------------------------------------------------------
// Traverses the scene and draws all models.
// --------------------------------------------------------
void Game::DrawScene(const dg::Scene& scene) {
  // Traverse scene tree and sort out different types of objects
  // into their own lists.
  std::forward_list<SceneObject*> remainingObjects;
  std::forward_list<Model*> models;
  std::forward_list<Light*> lights;
  remainingObjects.push_front((SceneObject*)&scene);
  while (!remainingObjects.empty()) {
    SceneObject *obj = remainingObjects.front();
    remainingObjects.pop_front();
    for (auto child = obj->Children().begin();
      child != obj->Children().end();
      child++) {
      if (!(*child)->enabled) continue;
      remainingObjects.push_front(child->get());
      if (auto model = std::dynamic_pointer_cast<Model>(*child)) {
        models.push_front(model.get());
      } else if (auto light = std::dynamic_pointer_cast<Light>(*child)) {
        lights.push_front(light.get());
      }
    }
  }

  // Set up light array.
  Light::ShaderData lightArray[Light::MAX_LIGHTS];
  int lightIdx = 0;
  for (auto light = lights.begin(); light != lights.end(); light++) {
    if (lightIdx >= Light::MAX_LIGHTS) {
      break;
    }
    lightArray[lightIdx++] = (*light)->GetShaderData();
  }

  // Projection matrices.
  std::shared_ptr<Camera> camera = scene.GetCamera();
  glm::mat4x4 view = camera->GetViewMatrix();
  glm::mat4x4 projection = camera->GetProjectionMatrix((float)width / height);

  // Render models.
  Transform camera_SS = camera->SceneSpace();
  for (auto model = models.begin(); model != models.end(); model++) {
    (*model)->Draw(
      view, projection, (glm::vec3&)camera_SS.translation, lightArray);
  }
}


#pragma region Mouse Input

// --------------------------------------------------------
// Helper method for mouse clicking.  We get this information
// from the OS-level messages anyway, so these helpers have
// been created to provide basic mouse input if you want it.
// --------------------------------------------------------
void Game::OnMouseDown(WPARAM buttonState, int x, int y)
{
  // Add any custom code here...

  // Save the previous mouse position, so we have it for the future
  prevMousePos.x = x;
  prevMousePos.y = y;

  // Caputure the mouse so we keep getting mouse move
  // events even if the mouse leaves the window.  we'll be
  // releasing the capture once a mouse button is released
  SetCapture(hWnd);
}

// --------------------------------------------------------
// Helper method for mouse release
// --------------------------------------------------------
void Game::OnMouseUp(WPARAM buttonState, int x, int y)
{
  // Add any custom code here...

  // We don't care about the tracking the cursor outside
  // the window anymore (we're not dragging if the mouse is up)
  ReleaseCapture();
}

// --------------------------------------------------------
// Helper method for mouse movement.  We only get this message
// if the mouse is currently over the window, or if we're 
// currently capturing the mouse.
// --------------------------------------------------------
void Game::OnMouseMove(WPARAM buttonState, int x, int y)
{
  // Add any custom code here...

  // Save the previous mouse position, so we have it for the future
  prevMousePos.x = x;
  prevMousePos.y = y;
}

// --------------------------------------------------------
// Helper method for mouse wheel scrolling.  
// WheelDelta may be positive or negative, depending 
// on the direction of the scroll
// --------------------------------------------------------
void Game::OnMouseWheel(float wheelDelta, int x, int y)
{
  // Add any custom code here...
}
#pragma endregion
