#include "Mesh.h"
#include <iostream>
#include <fstream>
#include <Exceptions.h>

// For the DirectX Math library
using namespace DirectX;

std::shared_ptr<dg::Mesh> dg::Mesh::Quad = nullptr;
std::shared_ptr<dg::Mesh> dg::Mesh::Cube = nullptr;
std::shared_ptr<dg::Mesh> dg::Mesh::Cylinder = nullptr;

std::unordered_map<std::string, std::weak_ptr<dg::Mesh>> dg::Mesh::fileMap;

void dg::Mesh::CreateDefaultMeshes(
  ID3D11Device *device, ID3D11DeviceContext *context) {

  assert(Quad == nullptr);
  Quad = CreateQuad(device, context);

  assert(Cube == nullptr);
  Cube = CreateCube(device, context);

  assert(Cylinder == nullptr);
  Cylinder = CreateCylinder(device, context, 36);
}

dg::Mesh::Mesh(ID3D11Device *device, ID3D11DeviceContext *context)
  : device(device), context(context) {}

dg::Mesh::Mesh(Mesh&& other) {
  *this = std::move(other);
}

dg::Mesh::~Mesh() {
  if (vertexBuffer != nullptr) {
    vertexBuffer->Release();
    vertexBuffer = nullptr;
  }

  if (indexBuffer != nullptr) {
    indexBuffer->Release();
    indexBuffer = nullptr;
  }
}

dg::Mesh& dg::Mesh::operator=(Mesh&& other) {
  swap(*this, other);
  return *this;
}

void dg::swap(dg::Mesh& first, dg::Mesh& second) {
  using std::swap;
  swap(first.device, second.device);
  swap(first.context, second.context);
  swap(first.vertices, second.vertices);
  swap(first.vertexMap, second.vertexMap);
  swap(first.indices, second.indices);
  swap(first.vertexBuffer, second.vertexBuffer);
  swap(first.indexBuffer, second.indexBuffer);
}

void dg::Mesh::AddQuad(
  Vertex v1, Vertex v2, Vertex v3, Vertex v4, Winding winding) {
  AddTriangle(v1, v2, v3, winding);
  AddTriangle(v1, v3, v4, winding);
}

void dg::Mesh::AddTriangle(Vertex v1, Vertex v2, Vertex v3, Winding winding) {
  // DirectX's default winding order is CW, and OpenGL is CCW. Since we
  // switched the handedness from left-hand (default) to right-hand, we
  // have to invert the winding order here. For that reason, when vertices
  // are passed in CW winding order (default for DirectX), we actually invert
  // the winding order.
  if (winding == Winding::CW) {
    AddVertex(v2);
    AddVertex(v1);
    AddVertex(v3);
  } else {
    AddVertex(v1);
    AddVertex(v2);
    AddVertex(v3);
  }
}

void dg::Mesh::AddVertex(Vertex vertex) {
  // Ensure that we haven't built the mesh's buffers yet.
  assert(vertexBuffer == nullptr);
  assert(indexBuffer == nullptr);

  int index;
  auto pair = vertexMap.find(vertex);
  if (pair != vertexMap.end()) {
    // Vertex already exists in the vertex list, so refer to its index.
    index = pair->second;
  } else {
    // Vertex does not exist, so add it to the list.
    vertices.push_back(vertex);
    index = (int)vertices.size() - 1;
    vertexMap[vertex] = index;
  }

  indices.push_back(index);
}

void dg::Mesh::FinishBuilding() {
  // Ensure that we haven't built the mesh's buffers yet.
  assert(vertexBuffer == nullptr);
  assert(indexBuffer == nullptr);

  D3D11_BUFFER_DESC vbd;
  vbd.Usage = D3D11_USAGE_IMMUTABLE;
  vbd.ByteWidth = (unsigned int)(sizeof(Vertex) * vertices.size());
  vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  vbd.CPUAccessFlags = 0;
  vbd.MiscFlags = 0;
  vbd.StructureByteStride = 0;

  D3D11_SUBRESOURCE_DATA initialVertexData;
  initialVertexData.pSysMem = vertices.data();

  device->CreateBuffer(&vbd, &initialVertexData, &vertexBuffer);

  D3D11_BUFFER_DESC ibd;
  ibd.Usage = D3D11_USAGE_IMMUTABLE;
  ibd.ByteWidth = (unsigned int)(sizeof(int) * indices.size());
  ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
  ibd.CPUAccessFlags = 0;
  ibd.MiscFlags = 0;
  ibd.StructureByteStride = 0;

  D3D11_SUBRESOURCE_DATA initialIndexData;
  initialIndexData.pSysMem = indices.data();

  device->CreateBuffer(&ibd, &initialIndexData, &indexBuffer);

  // We don't need these vectors and map any more, since the buffers
  // area already built.
  numIndices = (int)indices.size();
  vertices.clear();
  vertexMap.clear();
  indices.clear();
}

void dg::Mesh::Draw() const {
  assert(vertexBuffer != nullptr);
  assert(indexBuffer != nullptr);

  UINT stride = sizeof(Vertex);
  UINT offset = 0;
  context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
  context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

  context->DrawIndexed(numIndices, 0, 0);
}

std::shared_ptr<dg::Mesh> dg::Mesh::CreateQuad(
  ID3D11Device *device, ID3D11DeviceContext *context) {

  auto mesh = std::make_shared<Mesh>(device, context);
  float S = 0.5f; // half size

  mesh->AddQuad(
    // position       normal          texCoord
    { { -S, -S, +0 }, { +0, +0, +1 }, { 0, 0 } },
    { { -S, +S, +0 }, { +0, +0, +1 }, { 0, 1 } },
    { { +S, +S, +0 }, { +0, +0, +1 }, { 1, 1 } },
    { { +S, -S, +0 }, { +0, +0, +1 }, { 1, 0 } },
    Winding::CCW
  );

  mesh->FinishBuilding();

  return mesh;
}

std::shared_ptr<dg::Mesh> dg::Mesh::CreateCube(
  ID3D11Device *device, ID3D11DeviceContext *context) {

  auto mesh = std::make_shared<Mesh>(device, context);
  float S = 0.5f; // half size

  // Front
  mesh->AddQuad(
    // position       normal          texCoord
    { { -S, -S, +S }, { +0, +0, +1 }, { 0, 0 } },
    { { -S, +S, +S }, { +0, +0, +1 }, { 0, 1 } },
    { { +S, +S, +S }, { +0, +0, +1 }, { 1, 1 } },
    { { +S, -S, +S }, { +0, +0, +1 }, { 1, 0 } },
    Winding::CCW
  );

  //// Back
  mesh->AddQuad(
    // position       normal          texCoord
    { { -S, -S, -S }, { +0, +0, -1 }, { 1, 0 } },
    { { +S, -S, -S }, { +0, +0, -1 }, { 0, 0 } },
    { { +S, +S, -S }, { +0, +0, -1 }, { 0, 1 } },
    { { -S, +S, -S }, { +0, +0, -1 }, { 1, 1 } },
    Winding::CCW
  );

  // Left
  mesh->AddQuad(
    // position       normal          texCoord
    { { -S, -S, -S }, { -1, +0, +0 }, { 0, 0 } },
    { { -S, +S, -S }, { -1, +0, +0 }, { 0, 1 } },
    { { -S, +S, +S }, { -1, +0, +0 }, { 1, 1 } },
    { { -S, -S, +S }, { -1, +0, +0 }, { 1, 0 } },
    Winding::CCW
  );

  // Right
  mesh->AddQuad(
    // position       normal          texCoord
    { { +S, -S, +S }, { +1, +0, +0 }, { 0, 0 } },
    { { +S, +S, +S }, { +1, +0, +0 }, { 0, 1 } },
    { { +S, +S, -S }, { +1, +0, +0 }, { 1, 1 } },
    { { +S, -S, -S }, { +1, +0, +0 }, { 1, 0 } },
    Winding::CCW
  );

  // Top
  mesh->AddQuad(
    // position       normal          texCoord
    { { -S, +S, +S }, { +0, +1, +0 }, { 0, 0 } },
    { { -S, +S, -S }, { +0, +1, +0 }, { 0, 1 } },
    { { +S, +S, -S }, { +0, +1, +0 }, { 1, 1 } },
    { { +S, +S, +S }, { +0, +1, +0 }, { 1, 0 } },
    Winding::CCW
  );

  // Bottom
  mesh->AddQuad(
    // position       normal          texCoord
    { { -S, -S, -S }, { +0, -1, +0 }, { 0, 0 } },
    { { -S, -S, +S }, { +0, -1, +0 }, { 0, 1 } },
    { { +S, -S, +S }, { +0, -1, +0 }, { 1, 1 } },
    { { +S, -S, -S }, { +0, -1, +0 }, { 1, 0 } },
    Winding::CCW
  );

  mesh->FinishBuilding();

  return mesh;
}

std::shared_ptr<dg::Mesh> dg::Mesh::CreateCylinder(
  ID3D11Device *device, ID3D11DeviceContext *context, int subdivisions) {

  if (subdivisions < 3) {
    subdivisions = 3;
  }

  auto mesh = std::make_shared<Mesh>(device, context);
  float S = 0.5f; // half size

  float angleInterval = XMConvertToRadians(360) / (float)subdivisions;
  for (int i = 0; i < subdivisions; i++) {
    XMVECTOR leftLongitudeQuat = XMQuaternionRotationRollPitchYaw(
      0, angleInterval * i, 0);
    XMVECTOR rightLongitudeQuat = XMQuaternionRotationRollPitchYaw(
      0, angleInterval * (i + 1), 0);

    XMVECTOR leftNormal = XMVector3Rotate(
      XMVectorSet(0, 0, 1, 0), leftLongitudeQuat);
    XMVECTOR rightNormal = XMVector3Rotate(
      XMVectorSet(0, 0, 1, 0), rightLongitudeQuat);

    XMFLOAT3 fLeftNormal;
    XMStoreFloat3(&fLeftNormal, leftNormal);
    XMFLOAT3 fRightNormal;
    XMStoreFloat3(&fRightNormal, rightNormal);

    XMVECTOR bottomUnit = XMVectorSet(0, -S, +S, 1);
    XMVECTOR topUnit = XMVectorSet(0, +S, +S, 1);

    XMVECTOR bottomLeft = XMVector3Rotate(bottomUnit, leftLongitudeQuat);
    XMVECTOR bottomRight = XMVector3Rotate(bottomUnit, rightLongitudeQuat);
    XMVECTOR topLeft = XMVector3Rotate(topUnit, leftLongitudeQuat);
    XMVECTOR topRight = XMVector3Rotate(topUnit, rightLongitudeQuat);

    XMFLOAT3 fBottomLeft;
    XMStoreFloat3(&fBottomLeft, bottomLeft);
    XMFLOAT3 fBottomRight;
    XMStoreFloat3(&fBottomRight, bottomRight);
    XMFLOAT3 fTopLeft;
    XMStoreFloat3(&fTopLeft, topLeft);
    XMFLOAT3 fTopRight;
    XMStoreFloat3(&fTopRight, topRight);

    float uvBottomHeight = 1 / 3.f;
    float uvTopHeight = 1 / 3.f;
    XMFLOAT2 uvBottomLeft((float)i / subdivisions, uvBottomHeight);
    XMFLOAT2 uvBottomRight((float)(i + 1) / subdivisions, uvBottomHeight);
    XMFLOAT2 uvTopLeft((float)i / subdivisions, uvTopHeight);
    XMFLOAT2 uvTopRight((float)(i + 1) / subdivisions, uvTopHeight);

    // Side quad
    mesh->AddQuad(
      // position           normal        texCoord
      Vertex(fBottomLeft, fLeftNormal, uvBottomLeft),
      Vertex(fBottomRight, fRightNormal, uvBottomRight),
      Vertex(fTopRight, fRightNormal, uvTopRight),
      Vertex(fTopLeft, fLeftNormal, uvTopLeft),
      Winding::CW
    );

    XMFLOAT3 topCenter = { 0, +S, 0 };
    XMFLOAT3 bottomCenter = { 0, -S, 0 };

    float uvExtents = 1.f / 6;
    XMVECTOR uvTopCenter = XMVectorSet(1.f / 6, 5.f / 6, 0, 1);
    XMVECTOR uvBottomCenter = XMVectorSet(1.f / 6, 1.f / 6, 0, 1);

    XMVECTOR uvCircleTopLeft = uvTopCenter + uvExtents * XMVectorSet(
      -XMVectorGetX(leftNormal), XMVectorGetZ(leftNormal), 0, 0);
    XMVECTOR uvCircleTopRight = uvTopCenter + uvExtents * XMVectorSet(
      -XMVectorGetX(rightNormal), XMVectorGetZ(rightNormal), 0, 0);

    XMVECTOR uvCircleBottomLeft = uvBottomCenter - uvExtents * XMVectorSet(
      XMVectorGetX(leftNormal), XMVectorGetZ(leftNormal), 0, 0);
    XMVECTOR uvCircleBottomRight = uvBottomCenter - uvExtents * XMVectorSet(
      XMVectorGetX(rightNormal), XMVectorGetZ(rightNormal), 0, 0);

    XMFLOAT3 upNormal = { 0, +1, 0 };
    XMFLOAT3 downNormal = { 0, -1, 0 };
    XMFLOAT2 fUVTopCenter;
    XMStoreFloat2(&fUVTopCenter, uvTopCenter);
    XMFLOAT2 fUVTopLeft;
    XMStoreFloat2(&fUVTopLeft, uvCircleTopLeft);
    XMFLOAT2 fUVTopRight;
    XMStoreFloat2(&fUVTopRight, uvCircleTopRight);
    XMFLOAT2 fUVBottomCenter;
    XMStoreFloat2(&fUVBottomCenter, uvBottomCenter);
    XMFLOAT2 fUVBottomLeft;
    XMStoreFloat2(&fUVBottomLeft, uvCircleBottomLeft);
    XMFLOAT2 fUVBottomRight;
    XMStoreFloat2(&fUVBottomRight, uvCircleBottomRight);

    // Top triangle
    mesh->AddTriangle(
      // position  normal    texCoord
      Vertex(topCenter, upNormal, fUVTopCenter),
      Vertex(fTopLeft, upNormal, fUVTopLeft),
      Vertex(fTopRight, upNormal, fUVTopRight),
      Winding::CW
    );

    // Bottom triangle
    mesh->AddTriangle(
      // position     normal      texCoord
      Vertex(bottomCenter, downNormal, fUVBottomCenter),
      Vertex(fBottomRight, downNormal, fUVBottomRight),
      Vertex(fBottomLeft, downNormal, fUVBottomLeft),
      Winding::CW
    );
  }

  mesh->FinishBuilding();

  return mesh;
}

std::shared_ptr<dg::Mesh> dg::Mesh::LoadOBJ(const char *filename) {
  auto found = fileMap.find(filename);
  if (found != fileMap.end()) {
    std::shared_ptr<Mesh> mesh = found->second.lock();
    if (mesh == nullptr) {
      fileMap.erase(filename);
    } else {
      return mesh;
    }
  }

  auto mesh = std::make_shared<Mesh>(
    DXCore::Instance->device, DXCore::Instance->context);

  std::ifstream obj(filename, std::ifstream::binary);

  if (!obj.is_open()) {
    throw FileNotFoundException(filename);
  }

  std::vector<XMFLOAT3> positions;
  std::vector<XMFLOAT3> normals;
  std::vector<XMFLOAT2> uvs;
  std::string line;

  while (obj.good()) {
    std::getline(obj, line);

    if (line.empty()) continue;

    // Check the type of line
    if (line.at(0) == 'v' && line.at(1) == 'n') {
      // Read the 3 numbers directly into an XMFLOAT3
      XMFLOAT3 norm;
      sscanf_s(
        line.c_str(),
        "vn %f %f %f",
        &norm.x, &norm.y, &norm.z);

      // Add to the list of normals
      normals.push_back(norm);
    } else if (line.at(0) == 'v' && line.at(1) == 't') {
      // Read the 2 numbers directly into an XMFLOAT2
      XMFLOAT2 uv;
      sscanf_s(
        line.c_str(),
        "vt %f %f",
        &uv.x, &uv.y);

      // Add to the list of uv's
      uvs.push_back(uv);
    } else if (line.at(0) == 'v') {
      // Read the 3 numbers directly into an XMFLOAT3
      XMFLOAT3 pos;
      sscanf_s(
        line.c_str(),
        "v %f %f %f",
        &pos.x, &pos.y, &pos.z);

      // Add to the positions
      positions.push_back(pos);
    } else if (line.at(0) == 'f') {
      // Read the face indices into an array
      unsigned int i[12];
      int facesRead = sscanf_s(
        line.c_str(),
        "f %d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d",
        &i[0], &i[1], &i[2],
        &i[3], &i[4], &i[5],
        &i[6], &i[7], &i[8],
        &i[9], &i[10], &i[11]);

      // - Create the verts by looking up
      //    corresponding data from vectors
      // - OBJ File indices are 1-based, so
      //    they need to be adusted
      Vertex v1(
        positions[i[0] - 1],
        normals[i[2] - 1],
        uvs[i[1] - 1]);

      Vertex v2(
        positions[i[3] - 1],
        normals[i[5] - 1],
        uvs[i[4] - 1]);

      Vertex v3(
        positions[i[6] - 1],
        normals[i[8] - 1],
        uvs[i[7] - 1]);

      // The model is most likely in a right-handed space,
      // especially if it came from Maya.  We want to convert
      // to a left-handed space for DirectX.  This means we 
      // need to:
      //  - Invert the Z position
      //  - Invert the normal's Z
      //  - Flip the winding order
      // We also need to flip the UV coordinate since DirectX
      // defines (0,0) as the top left of the texture, and many
      // 3D modeling packages use the bottom left as (0,0)

      // Flip the UV's since they're probably "upside down"
      v1.texCoord.y = 1.0f - v1.texCoord.y;
      v2.texCoord.y = 1.0f - v2.texCoord.y;
      v3.texCoord.y = 1.0f - v3.texCoord.y;

      // Flip Z (LH vs. RH)
      v1.position.z *= -1.0f;
      v2.position.z *= -1.0f;
      v3.position.z *= -1.0f;

      // Flip normal Z
      v1.normal.z *= -1.0f;
      v2.normal.z *= -1.0f;
      v3.normal.z *= -1.0f;

      // Add triangle.
      mesh->AddTriangle(v1, v2, v3, Winding::CCW);

      // Was there a 4th face?
      if (facesRead == 12) {
        // Make the last vertex
        Vertex v4(
          positions[i[9] - 1],
          normals[i[11] - 1],
          uvs[i[10] - 1]);

        // Flip the UV, Z pos and normal
        v4.texCoord.y = 1.0f - v4.texCoord.y;
        v4.position.z *= -1.0f;
        v4.normal.z *= -1.0f;

        // Add a whole triangle.
        mesh->AddTriangle(v1, v3, v4, Winding::CCW);
      }
    }
  }

  obj.close();

  mesh->FinishBuilding();
  fileMap.insert_or_assign(filename, mesh);
  return mesh;
}
