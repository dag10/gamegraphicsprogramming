//
//  Material.cpp
//

#include "Material.h"

dg::Material::Material(Material& other) {
  this->shader = other.shader;
  this->properties = other.properties;
}

dg::Material::Material(Material&& other) {
  *this = std::move(other);
}

dg::Material& dg::Material::operator=(Material& other) {
  *this = Material(other);
  return *this;
}

dg::Material& dg::Material::operator=(Material&& other) {
  swap(*this, other);
  return *this;
}

void dg::swap(Material& first, Material& second) {
  using std::swap;
  swap(first.shader, second.shader);
  swap(first.properties, second.properties);
}

void dg::Material::SetProperty(const std::string& name, bool value) {
  MaterialProperty prop;
  prop.type = PROPERTY_BOOL;
  prop.value._bool = value;
  properties.insert_or_assign(name, prop);
}

void dg::Material::SetProperty(const std::string& name, int value) {
  MaterialProperty prop;
  prop.type = PROPERTY_INT;
  prop.value._int = value;
  properties.insert_or_assign(name, prop);
}

void dg::Material::SetProperty(const std::string& name, float value) {
  MaterialProperty prop;
  prop.type = PROPERTY_FLOAT;
  prop.value._float = value;
  properties.insert_or_assign(name, prop);
}

void dg::Material::SetProperty(const std::string& name, glm::vec2 value) {
  MaterialProperty prop;
  prop.type = PROPERTY_VEC2;
  prop.value._vec2 = value;
  properties.insert_or_assign(name, prop);
}

void dg::Material::SetProperty(const std::string& name, glm::vec3 value) {
  MaterialProperty prop;
  prop.type = PROPERTY_VEC3;
  prop.value._vec3 = value;
  properties.insert_or_assign(name, prop);
}

void dg::Material::SetProperty(const std::string& name, glm::vec4 value) {
  MaterialProperty prop;
  prop.type = PROPERTY_VEC4;
  prop.value._vec4 = value;
  properties.insert_or_assign(name, prop);
}

void dg::Material::SetProperty(const std::string& name, glm::mat4x4 value) {
  MaterialProperty prop;
  prop.type = PROPERTY_MAT4X4;
  prop.value._mat4x4 = value;
  properties.insert_or_assign(name, prop);
}

void dg::Material::ClearProperty(const std::string& name) {
  properties.erase(name);
}

void dg::Material::SendCameraPosition(glm::vec3 position) {
  shader->SetVec3("_CameraPosition", position);
}

void dg::Material::SendLights(
  const Light::ShaderData(&lights)[Light::MAX_LIGHTS]) {
  shader->SetData(Light::LIGHTS_ARRAY_NAME, lights);
}

void dg::Material::SendMatrixMVP(glm::mat4x4 mvp) {
  shader->SetMat4("_Matrix_MVP", mvp);
}

void dg::Material::SendMatrixM(glm::mat4x4 m) {
  shader->SetMat4("_Matrix_M", m);
}

void dg::Material::SendMatrixNormal(glm::mat4x4 normal) {
  shader->SetMat4("_Matrix_Normal", normal);
}

void dg::Material::Use() const {
  for (auto it = properties.begin(); it != properties.end(); it++) {
    switch (it->second.type) {
    case PROPERTY_BOOL:
      shader->SetInt(it->first, it->second.value._bool);
      break;
    case PROPERTY_INT:
      shader->SetInt(it->first, it->second.value._int);
      break;
    case PROPERTY_FLOAT:
      shader->SetFloat(it->first, it->second.value._float);
      break;
    case PROPERTY_VEC2:
      shader->SetVec2(it->first, it->second.value._vec2);
      break;
    case PROPERTY_VEC3:
      shader->SetVec3(it->first, it->second.value._vec3);
      break;
    case PROPERTY_VEC4:
      shader->SetVec4(it->first, it->second.value._vec4);
      break;
    case PROPERTY_MAT4X4:
      shader->SetMat4(it->first, it->second.value._mat4x4);
      break;
    default:
      break;
    }
  }

  shader->Use();
}

