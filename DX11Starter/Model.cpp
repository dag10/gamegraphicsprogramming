//
//  Model.cpp
//

#include <Model.h>
#include <Utils.h>

dg::Model::Model() : SceneObject() {}

dg::Model::Model(
  std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material,
  Transform transform)
  : mesh(mesh), material(material), SceneObject(transform) { }

dg::Model::Model(Model& other) : SceneObject(other) {
  this->mesh = other.mesh;
  this->material = other.material;
}

void dg::Model::Draw(
  const glm::mat4x4& view, const glm::mat4x4& projection,
  const glm::vec3& cameraPosition,
  const Light::ShaderData(&lights)[Light::MAX_LIGHTS]) const {
  glm::mat4x4 xfMat = SceneSpace().ToMat4();

  material->SendMatrixNormal(glm::transpose(glm::inverse(xfMat)));
  material->SendMatrixM(xfMat);
  material->SendMatrixMVP(xfMat * view * projection);
  material->SendCameraPosition(cameraPosition);
  material->SendLights(lights);

  material->Use();
  mesh->Draw();
}
