cbuffer Camera : register(b0) {
  matrix _Matrix_M;
  matrix _Matrix_MVP;
  matrix _Matrix_Normal;
};

struct VertexShaderInput {
  float3 position		: POSITION;
  float3 normal		: NORMAL;
  float3 texCoord		: TEXCOORD;
};

struct VertexToPixel {
  float4 position		: SV_POSITION;
  float4 scenePos		: POSITION;
  float4 normal		: NORMAL;
  float2 texCoord		: TEXCOORD;
};

VertexToPixel main(VertexShaderInput input) {
  VertexToPixel output;

  output.scenePos = mul(float4(input.position, 1.0f), _Matrix_M);
  output.position = mul(float4(input.position, 1.0f), _Matrix_MVP);
  output.normal = normalize(mul(input.normal, _Matrix_Normal));
  output.texCoord = input.texCoord;

  return output;
};
