//
//  Transform.cpp
//

#include <Transform.h>
#include <Utils.h>
#include <sstream>

// For the DirectX Math library
using namespace DirectX;

dg::Transform dg::Transform::T(XMFLOAT3 translation) {
  Transform xf;
  xf.translation = translation;
  return xf;
}

dg::Transform dg::Transform::R(XMFLOAT4 rotation) {
  Transform xf;
  xf.rotation = rotation;
  return xf;
}

dg::Transform dg::Transform::S(XMFLOAT3 scale) {
  Transform xf;
  xf.scale = scale;
  return xf;
}

dg::Transform dg::Transform::TR(XMFLOAT3 translation, XMFLOAT4 rotation) {
  Transform xf;
  xf.translation = translation;
  xf.rotation = rotation;
  return xf;
}

dg::Transform dg::Transform::RS(XMFLOAT4 rotation, XMFLOAT3 scale) {
  Transform xf;
  xf.rotation = rotation;
  xf.scale = scale;
  return xf;
}

dg::Transform dg::Transform::TS(XMFLOAT3 translation, XMFLOAT3 scale) {
  Transform xf;
  xf.translation = translation;
  xf.scale = scale;
  return xf;
}

dg::Transform dg::Transform::TRS(
  XMFLOAT3 translation, XMFLOAT4 rotation, XMFLOAT3 scale) {
  Transform xf;
  xf.translation = translation;
  xf.rotation = rotation;
  xf.scale = scale;
  return xf;
}

XMFLOAT4 dg::Transform::EulerToQuatRad(XMFLOAT3 euler) {
  XMFLOAT4 ret;
  XMStoreFloat4(
    &ret,
    DirectX::XMQuaternionRotationRollPitchYawFromVector(
      XMLoadFloat3(&euler)));
  return ret;
}

XMFLOAT4 dg::Transform::EulerToQuatDeg(XMFLOAT3 euler) {
  XMFLOAT4 ret;
  XMStoreFloat4(
    &ret,
    DirectX::XMQuaternionRotationRollPitchYawFromVector(
      XMVectorScale(XMLoadFloat3(&euler), XM_PI / 180.f)));
  return ret;
}

dg::Transform dg::Transform::Inverse() const {
  Transform xf;

  XMVECTOR vScale = XMVectorReciprocal(XMLoadFloat3(&scale));
  XMStoreFloat3(&xf.scale, vScale);

  XMVECTOR qRotation = DirectX::XMQuaternionInverse(XMLoadFloat4(&rotation));
  XMStoreFloat4(&xf.rotation, qRotation);

  XMVECTOR vTranslation = XMLoadFloat3(&translation);
  vTranslation = XMVectorNegate(XMVectorMultiply(vScale, vTranslation));
  vTranslation = XMVector3Rotate(vTranslation, qRotation);
  XMStoreFloat3(&xf.translation, vTranslation);

  return xf;
}

dg::Transform dg::operator*(const dg::Transform& a, const dg::Transform& b) {
  Transform xf;

  XMVECTOR vAScale = XMLoadFloat3(&a.scale);
  XMVECTOR vBScale = XMLoadFloat3(&b.scale);
  XMStoreFloat3(&xf.scale, XMVectorMultiply(vAScale, vBScale));

  XMVECTOR qARotation = XMLoadFloat4(&a.rotation);
  XMVECTOR qBRotation = XMLoadFloat4(&b.rotation);
  XMStoreFloat4(
    &xf.rotation, DirectX::XMQuaternionMultiply(qBRotation, qARotation));

  XMVECTOR vATranslation = XMLoadFloat3(&a.translation);
  XMVECTOR vBTranslation = XMLoadFloat3(&b.translation);
  XMStoreFloat3(
    &xf.translation, XMVectorAdd(
      vATranslation,
      XMVector3Rotate(
        XMVectorMultiply(vAScale, vBTranslation), qARotation)));

  return xf;
}

glm::mat4x4 dg::Transform::ToMat4() const {
  return FLOAT4X4toMAT4X4(ToFloat4X4());
}

XMFLOAT4X4 dg::Transform::ToFloat4X4() const {
  XMFLOAT4X4 ret;
  XMStoreFloat4x4(&ret, ToXMMatrix());
  return ret;
}

XMFLOAT3 dg::Transform::Right() const {
  XMFLOAT3 ret;
  XMStoreFloat3(
    &ret, XMVector3Rotate(XMLoadFloat3(&RIGHT), XMLoadFloat4(&rotation)));
  return ret;
}

XMFLOAT3 dg::Transform::Up() const {
  XMFLOAT3 ret;
  XMStoreFloat3(
    &ret, XMVector3Rotate(XMLoadFloat3(&UP), XMLoadFloat4(&rotation)));
  return ret;
}

XMFLOAT3 dg::Transform::Forward() const {
  XMFLOAT3 ret;
  XMStoreFloat3(
    &ret, XMVector3Rotate(XMLoadFloat3(&FORWARD), XMLoadFloat4(&rotation)));
  return ret;
}

std::string dg::Transform::ToString() const {
  std::stringstream ss;
  ss << *this;
  return ss.str();
}

std::ostream& dg::operator<<(std::ostream& os, const dg::Transform& xf) {
  os << "T: ";
  os << xf.translation.x << ", ";
  os << xf.translation.y << ", ";
  os << xf.translation.z << std::endl;

  os << "R (quat): ";
  os << xf.rotation.w << ", ";
  os << xf.rotation.x << ", ";
  os << xf.rotation.y << ", ";
  os << xf.rotation.z << std::endl;

  os << "S: ";
  os << xf.scale.x << ", ";
  os << xf.scale.y << ", ";
  os << xf.scale.z;

  return os;
}
