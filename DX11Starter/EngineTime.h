//
//  EngineTime.h
//
#pragma once

namespace dg {

  class Time {

  public:

    static float Elapsed;
    static float Delta;

  }; // class Time

} // namespace dg

