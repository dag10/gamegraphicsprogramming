//
//  Shader.h
//

#pragma once

#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include <glm/glm.hpp>
#include <Transform.h>
#include <SimpleShader.h>

namespace dg {

  // Copy is disabled, only moves are allowed. This prevents us
  // from leaking or redeleting the openGL shader program resource.
  class Shader {

  public:

    static Shader FromFiles(
      const std::string& vertexPath, const std::string& fragmentPath);

    Shader() = default;
    Shader(Shader& other) = delete;
    Shader(Shader&& other);
    virtual ~Shader() {}
    Shader& operator=(Shader& other) = delete;
    Shader& operator=(Shader&& other);
    friend void swap(Shader& first, Shader& second); // nothrow

    void Use();

    void SetBool(const std::string& name, bool value);
    void SetInt(const std::string& name, int value);
    void SetFloat(const std::string& name, float value);
    void SetVec2(const std::string& name, const glm::vec2& value);
    void SetVec3(const std::string& name, const glm::vec3& value);
    void SetVec4(const std::string& name, const glm::vec4& value);
    void SetMat4(const std::string& name, const glm::mat4& mat);
    void SetMat4(const std::string& name, const DirectX::XMFLOAT4X4& value);
    void SetMat4(const std::string& name, const Transform& xf);
    template <typename T>
    void SetData(const std::string& name, const T& data) {
      vertexShader->SetData(name, &data, sizeof(data));
      pixelShader->SetData(name, &data, sizeof(data));
    }

  private:

    std::shared_ptr<SimpleVertexShader> vertexShader = nullptr;
    std::shared_ptr<SimplePixelShader> pixelShader = nullptr;

    std::string vertexPath = std::string();
    std::string fragmentPath = std::string();

  }; // class Shader

} // namespace dg

