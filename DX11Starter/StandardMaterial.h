//
//  StandardMaterial.h
//

#pragma once

#include <memory>
#include "Material.h"
#include "Shader.h"
#include "SimpleShader.h"
#include <glm/glm.hpp>

namespace dg {

  class StandardMaterial : public Material {

  public:

    static StandardMaterial WithColor(glm::vec3 color);
    static StandardMaterial WithColor(glm::vec4 color);

    StandardMaterial();

    StandardMaterial(StandardMaterial& other);
    StandardMaterial(StandardMaterial&& other);
    StandardMaterial& operator=(StandardMaterial& other);
    StandardMaterial& operator=(StandardMaterial&& other);
    friend void swap(StandardMaterial& first, StandardMaterial& second); // nothrow

    void Use() const;

    void SetUVScale(glm::vec2 scale);
    void SetLit(bool lit);
    void SetDiffuse(float diffuse);
    void SetDiffuse(glm::vec3 diffuse);
    void SetSpecular(float specular);
    void SetSpecular(glm::vec3 specular);
    void SetShininess(float shininess);

  private:

    static std::shared_ptr<Shader> standardShader;

  }; // class Material

} // namespace dg
