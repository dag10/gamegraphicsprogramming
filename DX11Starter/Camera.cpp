//
//  Camera.cpp
//

#include "Camera.h"
#include <DirectXMath.h>
#include <Utils.h>

// For the DirectX Math library
using namespace DirectX;

dg::Camera::Camera() : SceneObject() {}

glm::mat4x4 dg::Camera::GetViewMatrix() const {
  return SceneSpace().Inverse().ToMat4();
}

glm::mat4x4 dg::Camera::GetProjectionMatrix(float aspectRatio) const {
  if (aspectRatio == 0) aspectRatio = 1;

  XMFLOAT4X4 ret;
  XMStoreFloat4x4(
    &ret, XMMatrixPerspectiveFovRH(fov, aspectRatio, nearClip, farClip));
  return FLOAT4X4toMAT4X4(ret);
}
