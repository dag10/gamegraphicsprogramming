//
//  StandardMaterial.cpp
//

#include "StandardMaterial.h"

// For the DirectX Math library
using namespace DirectX;

std::shared_ptr<dg::Shader> dg::StandardMaterial::standardShader = nullptr;

dg::StandardMaterial dg::StandardMaterial::WithColor(glm::vec3 color) {
  StandardMaterial material;
  material.SetDiffuse(color);
  return material;
}

dg::StandardMaterial dg::StandardMaterial::WithColor(glm::vec4 color) {
  StandardMaterial material;
  material.SetDiffuse({ color.x, color.y, color.z });
  return material;
}

dg::StandardMaterial::StandardMaterial() : Material() {
  if (standardShader == nullptr) {
    standardShader = std::make_shared<Shader>(dg::Shader::FromFiles(
      "StandardVertexShader.cso",
      "StandardPixelShader.cso"));
  }

  shader = StandardMaterial::standardShader;

  SetUVScale({ 1.f, 1.f });
  SetLit(true);
  SetDiffuse(1.0f);
  SetSpecular(0.0f);
  SetShininess(32.0f);
}

dg::StandardMaterial::StandardMaterial(StandardMaterial& other)
  : Material(other) {
}

dg::StandardMaterial::StandardMaterial(StandardMaterial&& other) {
  *this = std::move(other);
}

dg::StandardMaterial& dg::StandardMaterial::operator=(StandardMaterial& other) {
  *this = StandardMaterial(other);
  return *this;
}

dg::StandardMaterial& dg::StandardMaterial::operator=(StandardMaterial&& other) {
  swap(*this, other);
  return *this;
}

void dg::swap(StandardMaterial& first, StandardMaterial& second) {
  using std::swap;
  swap((Material&)first, (Material&)second);
}

void dg::StandardMaterial::Use() const {
  Material::Use();
}

void dg::StandardMaterial::SetUVScale(glm::vec2 scale) {
  SetProperty("uvScale", scale);
}

void dg::StandardMaterial::SetLit(bool lit) {
  SetProperty("lit", lit);
}

void dg::StandardMaterial::SetDiffuse(float diffuse) {
  SetDiffuse({ diffuse, diffuse, diffuse });
}

void dg::StandardMaterial::SetDiffuse(glm::vec3 diffuse) {
  SetProperty("diffuse", diffuse);
}

void dg::StandardMaterial::SetSpecular(float specular) {
  SetSpecular({ specular, specular, specular });
}

void dg::StandardMaterial::SetSpecular(glm::vec3 specular) {
  SetProperty("specular", specular);
}

void dg::StandardMaterial::SetShininess(float shininess) {
  SetProperty("shininess", shininess);
}

