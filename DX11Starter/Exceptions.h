//
//  Exceptions.h
//

#pragma once

#include <stdexcept>
#include <string>

namespace dg {

  class EngineError : public std::runtime_error {
  public:
    EngineError(const std::string& msg) : std::runtime_error(msg) {}
  };

  class FileNotFoundException : public EngineError {
  public:
    FileNotFoundException(const std::string& path)
      : EngineError("Unable to open \"" + path + "\"") { };
  };

  class ShaderLoadException : public EngineError {
  public:
    ShaderLoadException(const std::string& path)
      : EngineError("Failed to load shader \"" + path + "\"") {}
  };

  class ResourceLoadException : public EngineError {
  public:
    ResourceLoadException(const std::string& str) : EngineError(str) {}
  };

  class STBLoadError : public EngineError {
  public:
    STBLoadError(const std::string& path, const std::string& str)
      : EngineError("Failed to load \"" + path + "\": " + str) {};
  };

} // namespace dg

