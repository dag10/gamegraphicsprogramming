//
//  SceneObject.cpp
//

#include <SceneObject.h>
#include <cassert>

// For the DirectX Math library
using namespace DirectX;

dg::SceneObject::SceneObject(Transform transform) : transform(transform) {}

// Creates a copy of the object without copying its children, and without
// a parent.
dg::SceneObject::SceneObject(SceneObject& other) {
  this->transform = other.transform;
  this->enabled = other.enabled;
}

dg::Transform dg::SceneObject::SceneSpace() const {
  if (parent == nullptr) {
    return transform;
  }

  return parent->SceneSpace() * transform;
}

void dg::SceneObject::SetSceneSpace(Transform transform) {
  if (parent == nullptr) {
    this->transform = transform;
  } else {
    this->transform = parent->SceneSpace().Inverse() * transform;
  }
}

void dg::SceneObject::AddChild(std::shared_ptr<SceneObject> child) {
  AddChild(child, true);
}

void dg::SceneObject::AddChild(
  std::shared_ptr<SceneObject> child, bool preserveWorldSpace) {
  if (child->parent == this) return;
  if (child->parent != nullptr) {
    child->parent->children.erase(child);
  }
  children.insert(child);
  child->SetParent(this, preserveWorldSpace);
}

void dg::SceneObject::RemoveChild(std::shared_ptr<SceneObject> child) {
  if (child->parent != this) return;
  children.erase(child);
  child->SetParent(nullptr, true);
}

dg::SceneObject *dg::SceneObject::Parent() const {
  return parent;
}

std::set<std::shared_ptr<dg::SceneObject>> &dg::SceneObject::Children() {
  return children;
}

void dg::SceneObject::LookAt(const SceneObject& object) {
  LookAtPoint(object.SceneSpace().translation);
}

void dg::SceneObject::LookAtDirection(XMFLOAT3 direction) {
  XMVECTOR vDir = XMVector3Normalize(XMLoadFloat3(&direction));

  // If we're told to look straight up or down, we have to pick a yaw.
  // Let's just default to looking at the global forward direction.
  // TODO: Retain the previous yaw in this case.
  float vDirY = XMVectorGetY(vDir);
  if (vDirY == 1 || vDirY == -1) {
    vDir = XMVector3Normalize(XMVectorSet(0, vDirY, -0.0001f, 0));
  }

  XMFLOAT3 fDir;
  XMStoreFloat3(&fDir, vDir);

  XMFLOAT3 eulerOrientation = { 0, 0, 0 };

  float horizontalLen = XMVectorGetX(
    XMVector2Length(XMVectorSet(fDir.x, fDir.z, 0, 0)));
  eulerOrientation.x = atan(fDir.y / horizontalLen);

  eulerOrientation.y = -atan(fDir.x / -fDir.z);
  if (fDir.z >= 0) {
    eulerOrientation.y += XM_PI;
  }

  Transform xf_SS = SceneSpace();
  XMStoreFloat4(&xf_SS.rotation, XMQuaternionRotationRollPitchYawFromVector(
    XMLoadFloat3(&eulerOrientation)));
  SetSceneSpace(xf_SS);
}

void dg::SceneObject::LookAtPoint(XMFLOAT3 target) {
  Transform xf_SS = SceneSpace();
  XMFLOAT3 fDir;
  XMStoreFloat3(&fDir, XMVectorSubtract(
    XMLoadFloat3(&target), XMLoadFloat3(&xf_SS.translation)));
  LookAtDirection(fDir);
}

void dg::SceneObject::OrientUpwards() {
  LookAtDirection(SceneSpace().Forward());
}

// Assumes it was already added as a child to the new parent, and removed
// as a child from its old parent. This only sets the current parent field and
// updates the local transform.
void dg::SceneObject::SetParent(
  SceneObject *parent, bool preserveSceneSpace) {
  if (preserveSceneSpace) {
    Transform xf_SS = SceneSpace();
    this->parent = parent;
    SetSceneSpace(xf_SS);
  } else {
    this->parent = parent;
  }
}
